

TestSet


S.No.	Test Name	User Type	UserID	Pwd	Execute	Execute_HL	Environment	IE	Chrome	SAUCE_IE	SAUCE_CHROME
1	GLIS	PP_Int	zkfdbiy	1/O6RzIMuUyJJSpQl9fUJg==	Y	N	PP	N	N	N	Windows 7;60.0
2	GLIS	PP_Int	zk2umiw	1B3DA2C4	Y	N	PP	N	N	Windows 7;11.0	N
3	GLIS	PP_Ext	nbkots0	preprod1	Y	N	PP	N	N	N	Windows 7;60.0
4	GLIS	PP_Ext	nbkots0	preprod1	Y	N	PP	N	N	Windows 7;11.0	N




TestData

Sno	Test Name	Exe_PP_Int	Exe_PP_Ext	Exe_Prod_Int	Exe_Prod_Ext	Scenario navigation & title	Business Component	Param/Value	Param/Value	Param/Value	Param/Value	Param/Value
1	GLIS	Y	Y	Y	Y	Navigate		PageUrl				
2	GLIS	Y	Y	Y	Y	Navigate	Navigate_GLIS	offerings11#/tbill				
3	GLIS	Y	Y	Y	Y							
4	GLIS	Y	Y	Y	Y		Test	offerings11#/tbill				
												
												


TestNameMapping


AutomationString	ReportingTestName
SAUCE_CHROME#zk2umiw#1B3DA2C4#GLIS#PP_Int#Windows 7;60.0	GLIS verification for Internal user in PP (Chrome)
SAUCE_IE#zk2umiw#1B3DA2C4#GLIS#PP_Int#Windows 7;11.0	GLIS verification for Internal user in PP (IE)
SAUCE_CHROME#nbkots0#preprod1#GLIS#PP_Ext#Windows 7;60.0	GLIS verification for External user in PP (Chrome)
SAUCE_IE#nbkots0#preprod1#GLIS#PP_Ext#Windows 7;11.0	GLIS verification for External user in PP (IE)





												
												
//Package=java code//1stclass

package javaCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
//import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import testScripts.driverScript;
import testScripts.generateTestNG;

public class CommonFunctions {

	//Functions being used in the GLIS test scripts
	/**********************************************************************************************************/
	public static String getTimeStamp(String str_DateFormat){
		//"yyyy-MM-dd-HH-mm-ss"
		return new SimpleDateFormat(str_DateFormat).format(new Date());
	}
	
	public static String createAFolderInProjectDir(
			String str_FolderPathProperty, String str_FolderName)
			throws IOException	{

		File dir = new File(str_FolderPathProperty + str_FolderName);

		boolean successful = dir.mkdir();
		if (successful) {
			// creating the directory succeeded
			System.out.println("directory was created successfully");
		} else {
			// creating the directory failed
			System.out.println("failed trying to create the directory");
		}
		return str_FolderPathProperty + str_FolderName;
	}

	public static String convertToFileURL(String filename){
		
	    String path = new File(filename).getAbsolutePath();
	    if (File.separatorChar != '/')
	    {
	        path = path.replace(File.separatorChar, '/');
	    }
	    if (!path.startsWith("/"))
	    {
	        path = "/" + path;
	    }
	    String retVal =  "file:" + path;

	    return retVal;
	}

	public static String randomString(int len) {

		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		
		return sb.toString();
	}
	
	public static int randomNumberInRange(int min, int max){
		return (int) ((Math.random() * ((max - min) + 1)) + min);
	}
	
	public static String takeScreenShotMethod(WebDriver driver, String fileName) {

		FileInputStream readPropFile = null;

		String saveWithFileName = "";
		String screenFile = "";
		try {
			
			WebDriver augmentedDriver = new Augmenter().augment(driver);
			TakesScreenshot ts = (TakesScreenshot) augmentedDriver;
			byte[] image = ts.getScreenshotAs(OutputType.BYTES);

			SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMMyyyy_hhmmss");
			Date now = new Date();
			String timestamp = sdfDate.format(now);
			saveWithFileName = fileName + "_" +randomString(5) +"_"+ timestamp;
			saveWithFileName = saveWithFileName.trim().replaceAll("\\s+", "");

			System.out.println(driverScript.screenShotFolder  + "/" + saveWithFileName.replaceAll("\\s+", "") + ".jpg");
			File screenShot = new File(driverScript.screenShotFolder  + "/" + saveWithFileName.replaceAll("\\s+", "") + ".jpg");
				
			if (!screenShot.exists()) {
				screenShot.createNewFile();
			}
						
			screenFile = saveWithFileName + ".jpg";

			FileOutputStream fos = new FileOutputStream(screenShot);
			fos.write(image);
			fos.close();
			
		} catch (Exception exp) {
			exp.printStackTrace();
			Reporter.log("<br> <font color=red> Caught Exception While Capturing Screen shot </font> </br>");
		}
		// return saveWithFileName.concat(".jpg");
		return screenFile;

	}

	public String CreateDetailedFile(String str_Enviroment, String filePath, String fileName) throws IOException {

		File f = new File(filePath + fileName);
		// f.createNewFile();

		FileOutputStream fout = new FileOutputStream(f);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		XSSFWorkbook workBook = new XSSFWorkbook();

		XSSFSheet spreadSheet = workBook.createSheet(getTimeStamp("yyyyMMdd") + " " + str_Enviroment+" Report");

		// Style 3: Bold and text wrapping
		CellStyle style3 = workBook.createCellStyle();
		XSSFFont font3 = workBook.createFont();
		font3.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style3.setAlignment(CellStyle.ALIGN_CENTER);
		style3.setFont(font3);
		style3.setWrapText(true);

		spreadSheet.setColumnWidth(0, 6000);

		Row row2 = spreadSheet.createRow(0);

		// Cell 1
		Cell cell21 = row2.createCell(0);
		cell21.setCellValue("Test Navigation");
		row2.getCell(0).setCellStyle(style3);

		// Cell 2
		Cell cell22 = row2.createCell(1);
		cell22.setCellValue("Test Title");
		row2.getCell(1).setCellStyle(style3);

		// Cell 3
		Cell cell23 = row2.createCell(2);
		cell23.setCellValue("Status");
		row2.getCell(2).setCellStyle(style3);

		// Cell 4
		Cell cell24 = row2.createCell(3);
		cell24.setCellValue("Expected");
		row2.getCell(3).setCellStyle(style3);

		// Cell 5
		Cell cell25 = row2.createCell(4);
		cell25.setCellValue("Actual");
		row2.getCell(4).setCellStyle(style3);

		workBook.write(outputStream);

		outputStream.writeTo(fout);
		outputStream.close();
		fout.close();

		return filePath + fileName;

	}
	
	/**********************************************************************************************************/
	//Functions being used in the framework script
	/**********************************************************************************************************/
	public static void Login(WebDriver driver, String str_UserName,
			String str_password, String url, String str_userType) throws Exception {

		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream(generateTestNG.str_folderPath+"const.properties");
		prop.load(readPropFile);

		//String str_Node_f = null;
        //String str_Node = null;
        String str_Token = null;
        
		WebDriverWait wait2;

		try {
			WebElement element;
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("userid")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));

			element = driver.findElement(By.id("userid"));
			element.sendKeys(str_UserName);
			element = driver.findElement(By.id("password"));
			
			if(str_password.length()>10){
				element.sendKeys(Decrypt.DecryptData(str_password));
			}else{
				element.sendKeys(str_password);
			}
			
			element = driver.findElement(By.id("loginId"));
			element.click();

			try {
				
				Thread.sleep(2500);
				
				if(driver.getTitle().contains("Trading and Tools")){
					driver.findElement(By.id("ok")).click();
					Thread.sleep(2000);
				}
				
				wait.until(ExpectedConditions
						.titleContains("Two Factor - BAML Markets"));
				
				if(str_userType.equalsIgnoreCase("PP_Int_TestID")){
				
					WebDriver driver2 = null;
					driver2 = new ChromeDriver();
					WebElement element2;
	
					driver2.get(url);
					element2 = driver2.findElement(By.id("userid"));
					element2.sendKeys(prop.getProperty("USERNAME"));
					element2 = driver2.findElement(By.id("password"));
					element2.sendKeys(prop.getProperty("PASSWORD"));
					element2 = driver2.findElement(By.id("loginId"));
					Thread.sleep(2000);
					element2.click();
	
					Thread.sleep(1000);
	
					WebDriverWait wait3 = new WebDriverWait(driver2, 60);
					wait3.until(ExpectedConditions
							.titleContains("Two Factor - BAML Markets"));
	
					driver2.navigate().to(prop.getProperty("PP2F"));
					
					wait3.until(ExpectedConditions.presenceOfElementLocated(By
							.id("dynamicUserId")));
					CommonFunctions.getElementByID(driver2, "dynamicUserId")
							.sendKeys(str_UserName.toLowerCase());
					CommonFunctions.getElementByCSSSelector(driver2,
							"input[class='mercury-button']").click();
					Thread.sleep(2000);
	
					String tokenId = null;
					List<WebElement> list = CommonFunctions.getElementsByCSSSelector(driver2, "table[class='taglib-search-iterator'] tr[class*='portlet-section-']");
					for (WebElement row : list) {
						String rowData = row.getText();
						if (rowData.contains("False")) {
							tokenId = row.findElement(By.cssSelector("td[class*='align-left col-2 col-token']")).getText().trim();
							break;
						}
					}
					if (!url.isEmpty() && !url.equals("")) {
						driver2.close();
						Thread.sleep(2000);
						driver.navigate().to(url);
					}
					wait.until(ExpectedConditions
							.titleContains("Two Factor - BAML Markets"));
					WebElement tokenInputBox = CommonFunctions
							.getElementByCSSSelector(driver, "input[id='token']");
					if (tokenId != null) {
						tokenInputBox.sendKeys(tokenId);
					} else {
						System.out.println("Token is invalid");
					}
					WebElement okButton = CommonFunctions.getElementByCSSSelector(driver, "input[class='mercury-button']");
					okButton.click();
					
				}else if(str_userType.contains("Prod")){
				
                    String str_MainWindowHandle = driver.getWindowHandle();
                    System.out.println("Handle: " + str_MainWindowHandle);
//                  //Open a new tab 
//                  driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");       
//                  Thread.sleep(1000);
//                  
//                  ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
//                  
//                   driver.switchTo().window(tabs.get(1));
//                  driver.get("https://webmail.bankofamerica.com");

                    driver.navigate().to("https://mail.bankofamerica.com/owa");
                    /*
                     wait.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
                    
                    driver.findElement(By.id("username")).sendKeys("corp\\"+str_UserName);
                    Thread.sleep(100);
                    //System.out.println("Username Entered.....");
                                                
                    driver.findElement(By.id("password")).sendKeys(str_password);
                    Thread.sleep(100);

                    driver.findElement(By.cssSelector("input[value='Sign in']")).click();
                    Thread.sleep(1000);  
                    
                    //System.out.println("Password Entered.....");
                    
                    //str_MainWindowHandle = driver.getWindowHandle();
                    //System.out.println("Handle: " + str_MainWindowHandle);
                    
                    //HighlightElement.highlightMe(driver, driver.findElement(By.cssSelector("input[value='Sign in']")));
                    
                    //List<WebElement> lst_btns = driver.findElements(By.cssSelector("input[class*='btn']"));
//                  List<WebElement> lst_btns = driver.findElements(By.cssSelector("input[type='submit']"));                      
//                  System.out.println("Count.." + lst_btns.size());
//                  
//                  for (WebElement btn : lst_btns) {
//                           HighlightElement.highlightMe(driver, btn);
//                           System.out.println(btn.getAttribute("class"));
//                  }
//
//                  lst_btns.get(0).click();
                    
                    //driver.findElement(By.cssSelector("input[class*='btn']")).click();
                           //waitUntilReadyStateComplete(driver, 10000);
                           
//                  System.out.println("AFTER CLICK.....");
//                  Thread.sleep(1000);
//            
//                  System.out.println("BROWSER TITLE: " + driver.getTitle()); 
                    
                    
                    
                    driver.findElement(By.linkText("https://mail.bankofamerica.com/owa")).click();
                    */
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
                    
                    driver.findElement(By.id("username")).sendKeys("corp\\"+str_UserName);
                    Thread.sleep(100);
                    //System.out.println("Username Entered.....");
                                                
                    driver.findElement(By.id("password")).sendKeys(Decrypt.DecryptData(str_password));
                    Thread.sleep(100);

                    driver.findElement(By.className("signinbutton")).findElement(By.tagName("span")).click();
                    Thread.sleep(4000);
                    
                     wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[autoid='_lvv_9']")));
                    WebElement ele_User = driver.findElement(By.cssSelector("div[autoid='_lvv_9']"));
                    
                    List<WebElement> ele_divsUnderList = ele_User.findElements(By.cssSelector("div[id*='_ariaId_']"));
                    
                    String str_topEmailText = ele_divsUnderList.get(0).getText().substring(0,12);
                    System.err.println(str_topEmailText);
                    while(!(str_topEmailText.contains("BAML Markets"))){
                    	Thread.sleep(3000);

                    	System.out.println("Waiting for token Email....");
                    	
                    	str_topEmailText = ele_divsUnderList.get(0).getText().substring(0,12);
                    	
                    	if(str_topEmailText.contains("BAML Markets")){
                    		break;
                    	}
                    }
                    
                    ele_divsUnderList.get(0).click();
                    
                    Thread.sleep(2000);
                    
                    WebElement ele_document = driver.findElement(By.cssSelector("div[role='document']")).findElement(By.cssSelector("div[role='document']"));
                    
                    List<WebElement> ele_spanUnderList = ele_document.findElements(By.tagName("span"));
                    
                    for(WebElement ele_tokenSpan : ele_spanUnderList){
                		
                    	if(ele_tokenSpan.getText().contains("Your token is")){
                    		WebElement ele_parentStrong = ele_tokenSpan.findElement(By.xpath("./.."));
                    		WebElement ele_parentOfParentP = ele_parentStrong.findElement(By.xpath("./.."));
                    		
                    		String str_TokenText = ele_parentOfParentP.getText();
                       		
                    		str_Token = str_TokenText.trim().substring(str_TokenText.indexOf(":")+1, str_TokenText.length());
                    		
                    	}
                    	
                    }
                    
//                  System.out.println("User Name: " + ele_User.getText());
//                  if(ele_User.getText().equalsIgnoreCase("Kovvuri, Srini")) 
//                  {
                           /*List<WebElement> lst_Subjects = driver.findElements(By.id("divSubject"));
                           
                           if(lst_Subjects.size() > 0) 
                           {
                                  System.out.println("Total Subjects:" + lst_Subjects.size());
                                  for (int iSubj = 0; iSubj < lst_Subjects.size(); iSubj++) 
                                   {
                                        System.out.println(iSubj + ": " + lst_Subjects.get(iSubj).getText());
                                        
                                        if(!lst_Subjects.get(iSubj).getText().isEmpty() && lst_Subjects.get(iSubj).getText().equalsIgnoreCase("Bank of America Merrill Lynch Mercury® Portal Token"))
                                        {
                                           lst_Subjects.get(iSubj).click();
                                           Thread.sleep(1000);
                                           break;
                                        }
                                        
                                   }
                                  
                                   
                               wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[id='divBdy']")));
                                  
                                  //Click on 'click here' link
                                  if(CommonFunctions.IsElementExists(driver, "a[id='aIbBlk']")) 
                                  {
                                         driver.findElement(By.cssSelector("a[id='aIbBlk']")).click();
                                         Thread.sleep(3000);
                                  }      
                                  
                                  WebElement ele_Token = driver.findElement(By.xpath("//*[@id='divBdy']/div/div/span/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/font/table/tbody/tr/td/div[3]/font/span"));
                                  //System.out.println("TOKEN Text: " + ele_Token.getText());  //Your token is: 195462272
                                  String str_TokenText = ele_Token.getText();
                                  str_Token = str_TokenText.split(":")[1];
                                  //System.out.println("Token: " + str_Token.trim());

                        }
                           */
//                  }                    


//                  driver.switchTo().window(tabs.get(0));                               

                           
//                  driver.findElement(By.cssSelector("body")).sendKeys(Keys.ALT);
//                  driver.findElement(By.cssSelector("body")).sendKeys(Keys.ARROW_LEFT);
                    
                    driver.navigate().back();  Thread.sleep(1000);
                    driver.navigate().back(); Thread.sleep(1000);
                    driver.navigate().back(); Thread.sleep(1000);
                    driver.navigate().back();
                    
                    driver.get("https://markets.ml.com/two-factor?TARGET=/gsphome");
                    
                    System.out.println("TOKEN: " + str_Token);
                    
                    wait.until(ExpectedConditions.titleContains("Two Factor - BAML Markets"));
                    WebElement tokenInputBox=CommonFunctions.getElementByCSSSelector(driver,"input[id='token']");

                    try{
                       tokenInputBox.sendKeys(str_Token.trim());
                       WebElement okButton=CommonFunctions.getElementByCSSSelector(driver, "input[class='mercury-button']");
                       okButton.click();
                       CommonFunctions.waitUntil_jQueryActiveNReadyStateComplete(driver, 60000);

                    }catch(Exception e){
                    	e.printStackTrace();
                    	System.out.println("Token is invalid");
                    }
              
					
					
				}else{
					driver.navigate().to(prop.getProperty("PP2F"));
					
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dynamicUserId")));
					CommonFunctions.getElementByID(driver,"dynamicUserId").sendKeys(str_UserName.toLowerCase());
					CommonFunctions.getElementByCSSSelector(driver, "input[class='mercury-button']").click();
					Thread.sleep(2000);
				
					String tokenId=null;
					List<WebElement>list=CommonFunctions.getElementsByCSSSelector(driver, "table[class='taglib-search-iterator'] tr[class*='portlet-section-']");
					for(WebElement row:list)
					{
						String rowData=row.getText();
						if(rowData.contains("False"))
						{
							tokenId=row.findElement(By.cssSelector("td[class*='align-left col-2 col-token']")).getText().trim();
						    break;
						}
					}
					
				    if(!url.isEmpty() && !url.equals(""))
				    {
				    	driver.navigate().to(url);
				    }
				    wait.until(ExpectedConditions.titleContains("Two Factor - BAML Markets"));
				    WebElement tokenInputBox=CommonFunctions.getElementByCSSSelector(driver,"input[id='token']");   
				    if(tokenId != null)
				    {	
				    	tokenInputBox.sendKeys(tokenId);
				    }
				    else
				    {
				    	System.out.println("Token is invalid");
				    }
				    WebElement okButton=CommonFunctions.getElementByCSSSelector(driver, "input[class='mercury-button']");
				    okButton.click();
				}
			} catch (TimeoutException exp) {
		
			}
			
			try{
				wait2 = new WebDriverWait(driver, 15);
				wait2.until(ExpectedConditions.titleContains("Home - BAML Markets"));			
			}catch(Exception e){
				
			}
		} catch (NoSuchElementException exp) {
			exp.printStackTrace();
			try{
				wait2 = new WebDriverWait(driver, 15);
				wait2.until(ExpectedConditions.titleContains("Home - BAML Markets"));			
			}catch(Exception e){
				
			}
		}
	}
	
	public static void Logout(WebDriver driver, String str_Report)
	{
		
		try {
			driver.findElement(By.cssSelector("a[class='user-name-new']")).click();
			Thread.currentThread();
			Thread.sleep(200);
			
			driver.findElement(By.linkText("Log Out")).click();
			CommonFunctions.waitUntil_jQueryActiveNReadyStateComplete(driver, 10000);
			
			System.out.println(str_Report + " Logged out successfully");
			
		} catch (InterruptedException e) {
			System.out.println(str_Report + " Log out FAILED");
			e.printStackTrace();
		}
	}	
	
	public static String[] getArrayListItemsAsArray(ArrayList<String> arrayList) {
		try {
			String[] itemArray = new String[arrayList.size()];
			String[] returnedArray = arrayList.toArray(itemArray);
			return returnedArray;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Integer getCellIndexOfARow(String str_FileName,
			String str_SheetName, String str_filePath, int int_RowNum,
			String str_ColName) {

		Integer int_CellIndex = null;
		try {
			//Create an object of File class to open xlsx file
			//File file_TS =    new File(str_filePath+"\\"+str_FileName);
			File file_TS =    new File(str_filePath+str_FileName);
			
			//Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file_TS);
			Workbook myWorkBook = null;
			
			//Find the file extension by splitting file name in substring and getting only extension name
			String fileExtensionName = str_FileName.substring(str_FileName.indexOf("."));

			//Check condition if the file is xlsx file
			if(fileExtensionName.equals(".xlsx")){
			    //If it is xlsx file then create object of XSSFWorkbook class
				myWorkBook = new XSSFWorkbook(inputStream);
			}
			//Check condition if the file is xls file
			else if(fileExtensionName.equals(".xls")){
			    //If it is xls file then create object of XSSFWorkbook class
				myWorkBook = new HSSFWorkbook(inputStream);
			}				

			//Read sheet inside the workbook by its name
			Sheet mySheet_TS = myWorkBook.getSheet(str_SheetName);

			//Find number of rows in excel file
			//int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();
			
			int colCount_TS = mySheet_TS.getRow(0).getLastCellNum() - mySheet_TS.getRow(0).getFirstCellNum();
			
			for (int i = 0; i < colCount_TS; i++) 
			{
				 String str_CellData = mySheet_TS.getRow(0).getCell(i).getStringCellValue();
				 
				 if(str_CellData.trim().equalsIgnoreCase(str_ColName)) 
				 {
					 int_CellIndex = i;
					 break;
				 }
				 //System.out.println("Col: " + i + " ; " + str_CellData);
			}
			inputStream.close();
			myWorkBook = null;
			mySheet_TS = null;
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} 
		
		return int_CellIndex;
	}
	
	public static void waitUntil_jQueryActiveNReadyStateComplete(
			WebDriver driver, long waitTime_Millis) {

		/*
		 * Sample Outputs Current Time: 8 of 60 secs.; DOM Ready State:
		 * interactive; jQuery Active: 0 Current Time: 8 of 60 secs.; DOM Ready
		 * State: interactive; jQuery Active: 0 Current Time: 0 of 60 secs.; DOM
		 * Ready State: complete; jQuery Active: 1 Current Time: 0 of 60 secs.;
		 * DOM Ready State: loading; jQuery Active: 1 Current Time: 21 of 60
		 * secs.; DOM Ready State: loading; jQuery Active: 0 Current Time: 21 of
		 * 60 secs.; DOM Ready State: loading; jQuery Active: 0 Current Time: 21
		 * of 60 secs.; DOM Ready State: loading; jQuery Active: 0 Current Time:
		 * 21 of 60 secs.; DOM Ready State: complete; jQuery Active: 0 Current
		 * Time: 21 of 60 secs.; DOM Ready State: complete; jQuery Active: 0
		 * 
		 * Current Time: 6 of 60 secs.; DOM Ready State: loading; jQuery Active:
		 * true Current Time: 6 of 60 secs.; DOM Ready State: loading; jQuery
		 * Active: true Current Time: 6 of 60 secs.; DOM Ready State: loading;
		 * jQuery Active: true Current Time: 6 of 60 secs.; DOM Ready State:
		 * complete; jQuery Active: true
		 */

		long start_Time = 0;
		long end_Time = 0;
		long tot_Time = 0;
		String str_readyState = null;
		long jQuery_active = 0;

		boolean jQuery_IsReady = false;
		long jQuery_ready = 0;
		long jQuery_readyWait = 0;

		boolean bln_jQueryStatus = false;
		/*
		 * String jQuery_documentReady = null; ArrayList<String> status = new
		 * ArrayList<String>();
		 */

		try {
			start_Time = System.currentTimeMillis();
			str_readyState = (String) ((JavascriptExecutor) driver)
					.executeScript(("return document.readyState"));
			jQuery_active = (Long) ((JavascriptExecutor) driver)
					.executeScript(("return jQuery.active"));

			// System.out.println("Ajax.activeRequestCount: " + (long)
			// ((JavascriptExecutor)
			// driver).executeScript(("return Ajax.activeRequestCount")));
			// System.out.println("dojo.io.XMLHTTPTransport.inFlight.length: " +
			// (long) ((JavascriptExecutor)
			// driver).executeScript(("return dojo.io.XMLHTTPTransport.inFlight.length")));

			// System.out.println("From Funct: jQuery: " + jQuery_active +
			// " ReadyState: " + str_readyState);

			// Need to wait for Javascript and jQuery to finish loading.
			// Execute Javascript to check if jQuery.active is 0 and
			// document.readyState is complete, (jQuery.active give Active
			// connections to the server at present....we need to wait until it
			// become 0
			// which means the JS and jQuery load is complete.

			// jQuery Active = 0 means Page is loaded completely.

			// while (((jQuery_active!=0) ||
			// (!str_readyState.equalsIgnoreCase("complete"))) && tot_Time <
			// waitTime_Millis)
			while (((!bln_jQueryStatus) || (!str_readyState
					.equalsIgnoreCase("complete")))
					&& tot_Time < waitTime_Millis) {
				Thread.currentThread();
				Thread.sleep(500);
				end_Time = System.currentTimeMillis();
				tot_Time = end_Time - start_Time;

				str_readyState = (String) ((JavascriptExecutor) driver)
						.executeScript(("return document.readyState"));

				// jQuery_active = (long) ((JavascriptExecutor)
				// driver).executeScript(("return jQuery.active"));

				bln_jQueryStatus = (Boolean) ((JavascriptExecutor) driver)
						.executeScript(("return jQuery.active==0"));

				// jQuery_IsReady = (boolean) ((JavascriptExecutor)
				// driver).executeScript(("return jQuery.isReady"));
				// jQuery_readyWait = (long) ((JavascriptExecutor)
				// driver).executeScript(("return jQuery.readyWait"));
				// jQuery_documentReady = (String) ((JavascriptExecutor)
				// driver).executeScript(("return $(document).ready()"));

				// status = (ArrayList<String>) ((JavascriptExecutor)
				// driver).executeScript(("return $(document).ready()"));

				
				 System.out.println("Current Time: " +
				 TimeUnit.MILLISECONDS.toSeconds(tot_Time) + " of " +
				 TimeUnit.MILLISECONDS.toSeconds(waitTime_Millis) + " secs." +
				 "; DOM Ready State: " + str_readyState + "; jQuery Active: "
				 + bln_jQueryStatus);
				 

				/*
				 * System.out.println("Current Time: " +
				 * TimeUnit.MILLISECONDS.toSeconds(tot_Time) + " of " +
				 * TimeUnit.MILLISECONDS.toSeconds(waitTime_Millis) + " secs." +
				 * "; DOM Ready State: " + str_readyState + "; jQuery Active: "
				 * + jQuery_active);
				 */

				/*
				 * System.out.println("Current Time: " +
				 * TimeUnit.MILLISECONDS.toSeconds(tot_Time) + " of " +
				 * TimeUnit.MILLISECONDS.toSeconds(waitTime_Millis) + " secs." +
				 * "; DOM Ready State: " + str_readyState +
				 * //"; jQuery Active: " + bln_jQueryStatus +
				 * "; bln_jQueryStatus: " + bln_jQueryStatus +
				 * //"; jQuery IsReady: " + jQuery_IsReady +
				 * //"; jQuery readyWait: " + jQuery_readyWait +
				 * //"; jQuery_documentReady: "+ jQuery_documentReady) ;
				 * "; status size: " + status.size()+ "; status: "+
				 * status.get(0)) ;
				 */

			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
		} catch (Exception e) {

		}

	}
	public static WebDriver openBrowser_SAUCE(String str_SauceAccount,
			String str_BrowserType, String str_Version, String str_Platform,
			String str_URL, String str_deviceName,
			String str_deviceOrientation, String str_TestName,
			String str_UserName) throws InterruptedException, IOException {
		
		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream(generateTestNG.str_folderPath+"const.properties");
		prop.load(readPropFile);
		
		long startTime = System.currentTimeMillis();
		
		//WebDriver driver = null;
		RemoteWebDriver driver = null;
		WebDriverWait wait;
		String SAUCELABS_USERNAME = null;
		String SAUCELABS_ACCESSKEY = null;	
		
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		
/*		System.out.println("<<<<<<<<<< HELLO I AM FROM openBrowser_SAUCE FUNCTION >>>>>>>>>>>");
		System.out.println("str_BrowserType: " + str_BrowserType);
		System.out.println("str_Version: " + str_Version);
		System.out.println("str_Platform: " + str_Platform);		
		System.out.println("str_URL: " + str_URL);*/		

		if (str_SauceAccount.equalsIgnoreCase(prop.getProperty("SAUCEUSEID"))) {
			
			SAUCELABS_USERNAME = prop.getProperty("SAUCELABSUSERNAME");
			SAUCELABS_ACCESSKEY = prop.getProperty("SAUCELABSACCESSKEY");
			
		}
		
		String SAUCELABS_DRIVER_URL = "http://"
									  + SAUCELABS_USERNAME + ":" + SAUCELABS_ACCESSKEY
				                      //+"@ondemand.saucelabs.com:80/wd/hub";
        							  //+ "@localhost:443/wd/hub";			
		  							  + "@171.141.204.197:443/wd/hub";
		
/*		System.err.println("**************************************");
		System.err.println(SAUCELABS_DRIVER_URL);
		System.err.println("**************************************");
*/		//DesiredCapabilities capabilities = new DesiredCapabilities();
		
		switch (str_BrowserType.trim().toUpperCase()) {
			case "IE":
				//System.out.println("From IE case: " + str_BrowserType.trim().toUpperCase());
				
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			    capabilities.setCapability("platform", str_Platform);
			    capabilities.setCapability("version", str_Version);
			    //capabilities.setCapability("public", "");
			    //capabilities.setCapability("screenResolution", "1024x768");
			    capabilities.setCapability("screen-resolution","1280x1024");
			    capabilities.setCapability("name", str_TestName);
			    capabilities.setCapability("maxDuration",10800); //3Hrs  				    
			    capabilities.setCapability("idleTimeout",180); 
			    capabilities.setCapability("avoidProxy",true);   //https://wiki.saucelabs.com/display/DOCS/Test+Configuration+Options#TestConfigurationOptions-AvoidingtheSeleniumProxy
			    
/*			    		
 * 			    		
					browserAttachTimeout=0
					browserName=internet explorer
					cssSelectorsEnabled=true
					elementScrollBehavior=0
					enableElementCacheCleanup=true
					enablePersistentHover=true
					hasMetadata=true
					ie.browserCommandLineSwitches=
					ie.enableFullPageScreenshot=true
					ie.ensureCleanSession=false
					ie.fileUploadDialogTimeout=3000
					ie.forceCreateProcessApi=false
					ie.forceShellWindowsApi=false
					ignoreProtectedModeSettings=false
					ignoreZoomSetting=false
					initialBrowserUrl=about:blank
					javascriptEnabled=true
					nativeEvents=true
					pageLoadStrategy=normal
					platform=WINDOWS
					requireWindowFocus=false
					takesScreenshot=true
					unexpectedAlertBehaviour=dismiss
					version=11
					webdriver.remote.sessionid=de99fc3ae08f45c7a64d0023a3c299f5
			    					    
*/			    
			    
				try {
					driver = new RemoteWebDriver(new URL(SAUCELABS_DRIVER_URL),capabilities);
				
					
					driver.get(str_URL);
					
				} 
				catch (MalformedURLException e) {
					e.printStackTrace();
				}		
				break;
			case "CHROME":
				DesiredCapabilities capabilities1 = DesiredCapabilities.chrome();	
			    capabilities1.setCapability("platform", str_Platform);
			    capabilities1.setCapability("version", str_Version);
			    //capabilities.setCapability("public", "");
			    //capabilities.setCapability("screenResolution", "1024x768");
			    capabilities1.setCapability("screen-resolution","1280x1024");
			    capabilities1.setCapability("name", str_TestName);
			    //capabilities1.setCapability("idleTimeout",3600000);  // 1hr
			    capabilities1.setCapability("maxDuration",10800); //3Hrs 			    
			    capabilities1.setCapability("idleTimeout",180);  
			    capabilities1.setCapability("timeZone","New_York");
			    capabilities1.setCapability("avoidProxy",true);			    
			    //capabilities1.setCapability("nativeEvents",true);	
			    //capabilities1.setCapability("enablePersistentHover", false); 
			    
				 ChromeOptions options = new ChromeOptions();
				 //options.addArguments("chrome.switches", "--disable-extensions");
				 options.addArguments("chrome.switches", "--disable-extensions");	
				 
				 capabilities1.setCapability(ChromeOptions.CAPABILITY, options);			    
			    
				try {

					driver = new RemoteWebDriver(new URL(SAUCELABS_DRIVER_URL),capabilities1);
					//driver.setFileDetector(new LocalFileDetector());
					
					driver.get(str_URL);
				} 
				catch (MalformedURLException e) {
					e.printStackTrace();
				}		
				
				break;
//			case "FF":
//				DesiredCapabilities ff_Cap = DesiredCapabilities.firefox();	
//				ff_Cap.setCapability("version", "7");
//			    ff_Cap.setCapability("platform", Platform.WINDOWS);
//			    ff_Cap.setCapability("selenium-version", "2.46.0");
//				ff_Cap.setCapability("name", "Remote File Upload using Selenium 2's FileDetectors");			    
//				try {
//					RemoteWebDriver driver1 = new RemoteWebDriver(new URL(SAUCELABS_DRIVER_URL),ff_Cap);
//					driver1.setFileDetector(new LocalFileDetector());
//					driver1.get("http://sso.dev.saucelabs.com/test/guinea-file-upload");
//			        WebElement upload = driver1.findElement(By.id("myfile"));
//			        upload.sendKeys("C:\\working\\TradeUploadFile.xls");
//			        driver1.findElement(By.id("submit")).click();
//			        driver1.findElement(By.tagName("img"));
//			        //Assert.assertEquals("darkbulb.jpg (image/jpeg)", driver1.findElement(By.tagName("p")).getText());					
//					
//					
//				} 
//				catch (MalformedURLException e) {
//					e.printStackTrace();
//				}		
//				
//				break;				
			default:
			break;
		}
		
		long endTime = System.currentTimeMillis();
		long totLoadTime = endTime-startTime; 
		
		String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms").format(new Date());
		System.out.println("User: ["+ str_UserName + "]; Load Time to Open Browser " + 
						   str_BrowserType + " in Millis: " + 
						   totLoadTime + "; In Secs: " + TimeUnit.MILLISECONDS.toSeconds(totLoadTime) + 
						   "; TimeStamp: [" + timeStamp + "]");
		
		Reporter.log("<br> <font color=green>"+ 
						   "User: ["+ str_UserName + "]; Load Time to Open Browser " + 
						   str_BrowserType + " in Millis: " + 
						   totLoadTime + "; In Secs: " + TimeUnit.MILLISECONDS.toSeconds(totLoadTime) + 
						   "; TimeStamp: [" + timeStamp + "]" + 
					"</font> </br>");
		
		return driver;
	}
	public static WebDriver openBrowser(String str_BrowserType, String str_URL)
			throws InterruptedException, IOException {

		Properties prop = new Properties();
		FileInputStream readPropFile = null;
		readPropFile = new FileInputStream(generateTestNG.str_folderPath+"const.properties");
		prop.load(readPropFile);
		
		WebDriver driver = null;
		
		String ieExecutable = generateTestNG.str_folderPath+"/IEDriverServer.exe";
		String firefoxExecutable = "C:/Program Files (x86)/Mozilla Firefox/firefox.exe";
		String chromeExecutable = generateTestNG.str_folderPath+"/chromedriver.exe";

		if (str_BrowserType.equalsIgnoreCase("IE")) {

			System.out
					.println("<<<<<<<<<< HELLO I AM FROM openBrowser FUNCTION >>>>>>>>>>>");

			File file = new File(ieExecutable);
			System.setProperty("org.uncommons.reportng.escape-output", "false");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			// driver = new InternetExplorerDriver();
			DesiredCapabilities capabilities = DesiredCapabilities
					.internetExplorer();
			capabilities
					.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
			capabilities.setCapability(
					InternetExplorerDriver.INITIAL_BROWSER_URL, "");
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability("ignoreZoomSetting", true);
			capabilities.setCapability("ie.setProxyByServer", true);
			capabilities.setCapability("ensureCleanSession", true);
			capabilities.setCapability("nativeEvents", false); // false
			capabilities.setCapability("requireWindowFocus", true);
			capabilities.setCapability("enablePersistentHover", false); // Default
																		// is
																		// True
																		// but
																		// set
																		// it to
																		// false
			// capabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS,
			// true); // Set TabProcGrowth to 1
			// TabProcGrowth -- 0 --> 64Bit
			// TabProcGrowth -- 1 --> 32Bit >> Error : 'Access denied'

			// https://code.google.com/p/selenium/wiki/DesiredCapabilities

			driver = new InternetExplorerDriver(capabilities);

			Thread.sleep(5000);
			// System.out.println(driver.getCurrentUrl());
			driver.manage().window().maximize();
			Thread.sleep(5000);
			driver.get(str_URL);
			// CommonFunctions.homePage(driver, id, password,url,
			// env,LoginFilePath);

		} else if (str_BrowserType.equalsIgnoreCase("FF")) {

			// System.out.println("I am from Fire Fox block");

			System.out
					.println("<<<<<<<<<< HELLO I AM FROM openBrowser FUNCTION >>>>>>>>>>>");

			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("nativeEvents", false);
			capabilities.setCapability("ensureCleanSession", false);

			File pathToBinary = new File(
					"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
			FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);

			// Load Custom FF profile
			ProfilesIni profile = new ProfilesIni();
			FirefoxProfile myprofile = profile.getProfile("AutomationProfile");

			System.setProperty("org.uncommons.reportng.escape-output", "false");

			// FirefoxProfile firefoxProfile = new FirefoxProfile();
			driver = new FirefoxDriver(ffBinary, myprofile, capabilities);
			// driver = new FirefoxDriver(ffBinary,myprofile);
			driver.manage().window().maximize();
			driver.get(str_URL);

		} else if (str_BrowserType.equalsIgnoreCase("Chrome")) {

			System.out
					.println("<<<<<<<<<< HELLO I AM FROM openBrowser FUNCTION (Chrome) >>>>>>>>>>>");

			// File file = new File("C:\\ChromeDriver\\IEDriverServer.exe");
			File file = new File(chromeExecutable);
			System.setProperty("webdriver.chrome.driver",
					file.getAbsolutePath());
			
			System.setProperty("webdriver.chrome.driver", chromeExecutable);
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability("nativeEvents", false);
			
			capabilities.setCapability("ensureCleanSession", false);
			ChromeOptions o = new ChromeOptions();
			
			LoggingPreferences logPrefs = new LoggingPreferences();
		
			logPrefs.enable(LogType.BROWSER, Level.ALL);
			
			capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			// o.addArguments("disable-extensions");
			o.addArguments("--start-maximized");
			// o.setExperimentalOption("useAutomationExtension", false);
			driver = new ChromeDriver(o);
			// driver.manage().window().maximize();
			driver.get(str_URL);
		} else if (str_BrowserType.equalsIgnoreCase("Headless")) {
			driver = new HtmlUnitDriver();
			driver.get(str_URL);

		} else if (str_BrowserType.equalsIgnoreCase("SAUCE")) {
			String SAUCELABS_USERNAME = prop.getProperty("SAUCELABSUSERNAME");
			String SAUCELABS_ACCESSKEY = prop.getProperty("SAUCELABSACCESSKEY");
			String SAUCELABS_DRIVER_URL = "http://" + SAUCELABS_USERNAME + ":"
					+ SAUCELABS_ACCESSKEY
					// +"@ondemand.saucelabs.com:80/wd/hub";
					+ "@localhost:443/wd/hub";

			DesiredCapabilities capabilities = DesiredCapabilities
					.internetExplorer();

			capabilities.setCapability("platform", "Windows 7");
			capabilities.setCapability("version", "11.0");
			capabilities.setCapability("public", "");

			/*
			 * capabilities.setCapability("platform", "Windows 10");
			 * capabilities.setCapability("version", "11.103");
			 */

			// capabilities.setCapability("screenResolution", "1024x768");
			capabilities.setCapability("name",
					"Srini-Test from Hybrid Framework");

			try {
				driver = new RemoteWebDriver(new URL(SAUCELABS_DRIVER_URL),
						capabilities);
				driver.get(str_URL);
			} catch (MalformedURLException e) {

				e.printStackTrace();
			}

		}

		return driver;
	}
	/* Getting Element By CSS Selector */
	public static WebElement getElementByCSSSelector(WebDriver driver,
			String cssselector) {
		WebElement element = driver.findElement(By.cssSelector(cssselector));
		return element;
	}

	/* Getting Elements By CSS Selector */
	public static List<WebElement> getElementsByCSSSelector(WebDriver driver,
			String cssselector) {
		List<WebElement> elementsList = driver.findElements(By
				.cssSelector(cssselector));
		return elementsList;
	}

	/* Getting Element By ID */
	public static WebElement getElementByID(WebDriver driver, String idName) {
		WebElement element = driver.findElement(By.id(idName));
		return element;

	}
	
	public static <T> Object returnValueFromMap(Map<T, T> genericMap, Object key){
		
		Iterator it = genericMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        if(pair.getKey().equals(key)){
	        	return pair.getValue();
	        }
	    }

	    return null;
	}

	public static String returnValueFromMap(Map<String, String> genericMap, String key){
		
		Iterator<Entry<String, String>> it = genericMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        if(pair.getKey().equals(key)){
	        	return (String) pair.getValue();
	        }
	    }

	    return null;
	}

	public static void printTestDescription(String description) {

		Reporter.log("<br> <font color=blue> " + description + "</font></br>");
	}
	/**********************************************************************************************************/

}



package javaCode;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

// Run TrippleDes.java file and give the userid or password to get the encrypt format.


public class Decrypt{

    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;

    public Decrypt() throws Exception {
        myEncryptionKey = "ThisIsSpartaThisIsSparta";
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }


    public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return encryptedString;
    }


    public String decrypt(String encryptedString) {
        String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }


    public static String DecryptData(String encryptedData) throws Exception
    {
        Decrypt td= new Decrypt();
        String decrypted=td.decrypt(encryptedData);
//        System.out.println("Decrypted String:" + decrypted);
        return decrypted;

    }

}

//3rd class
package javaCode;

public class GenericPair<K, V> {

	    private K key;

	    private V value;

	    public GenericPair(K key, V value) {
	        this.key = key;
	        this.value = value;
	    }

	    public K getKey() {
	        return key;
	    }

	    public void setKey(K key) {
	        this.key = key;
	    }

	    public V getValue() {
	        return value;
	    }

	    public void setValue(V value) {
	        this.value = value;
	    }

	   /* How to use:
	    * public static void main(String[] args) {
	    	WebElement x = null;
	        GenericPair<WebElement, String> pair = new GenericPair<WebElement, String>(x, "value");
	        System.out.println(pair.getKey());
	        System.out.println(pair.getValue());
	    }*/

}
											
												
												
												
	//4th class
package javaCode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import objectRepository.ObjFunctions;
import objectRepository.Objects;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import reporting.Report;
import testScripts.driverScript;

public class GLISCommFuncs {
	
	private static void fail(WebDriver driver, String str_UserData, String fileName,
			String str_TestNavigation, String str_TestTitle,
			String str_Expected, String str_Actual)
			throws InvalidFormatException, IOException {
		String str_fileName = fileName;
		String str_screenShotFileName = CommonFunctions.takeScreenShotMethod(driver, str_fileName);
		Report.failDetailedReporting(driver, str_screenShotFileName, str_TestNavigation, str_TestTitle, "Fail", str_Expected, str_Actual);
		Report.Log_ReportNG("FAIL", str_screenShotFileName, str_TestNavigation, str_TestTitle, str_Expected, str_Actual);
		System.err.println(str_TestNavigation+ " : " +str_TestTitle+ " : " +"Fail"+ " : " +str_Expected+ " : " +str_Actual);
		int count = 0;
		
		if(driverScript.failedStepsMap.containsKey(str_UserData)){
			count = driverScript.failedStepsMap.get(str_UserData);
			driverScript.failedStepsMap.remove(str_UserData);
			driverScript.failedStepsMap.put(str_UserData, count + 1);
		}

	}

	private static void pass(WebDriver driver, String str_UserData,  String str_TestNavigation,
			String str_TestTitle) throws InvalidFormatException, IOException {
		Report.Log_ReportNG("PASS", "", str_TestNavigation, str_TestTitle, "Pass");
		Report.passDetailedReporting(str_TestNavigation, str_TestTitle, "Pass");
		System.out.println(str_TestNavigation+" : "+str_TestTitle+" : Pass");
		int count = 0;
		
		if(driverScript.passedStepsMap.containsKey(str_UserData)){
			count = driverScript.passedStepsMap.get(str_UserData);
			driverScript.passedStepsMap.remove(str_UserData);
			driverScript.passedStepsMap.put(str_UserData, count + 1);
		}

	}
	
	public static void test(WebDriver driver, String str_UserData, String str_URL, String str_TestTitle){
		
		System.err.println(driver.getTitle() + " : "+str_TestTitle);
	}
	
	
}


//

package javaCode;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
    
public class HighlightElement{
	
	 public static void highlightMe(WebDriver driver,WebElement element) throws InterruptedException{
		  //Creating JavaScriptExecuter Interface
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   for (int iCnt = 0; iCnt < 5; iCnt++) {
		      //Execute javascript
		         js.executeScript("arguments[0].style.border='4px groove red'", element);
		         Thread.sleep(400);
		         js.executeScript("arguments[0].style.border=''", element);
		   }
		 }	

}


//next class

package javaCode;
import java.security.spec.KeySpec;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

public class TrippleDes {
	
	
	


    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";//....
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;

    public TrippleDes() throws Exception {
        myEncryptionKey = "ThisIsSpartaThisIsSparta";
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }


    public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return encryptedString;
    }


    public String decrypt(String encryptedString) {
        String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }


    public static void main(String args []) throws Exception
    {
        TrippleDes td= new TrippleDes();
        System.out.println(" Hello Enter the data to be encrypted below !!!!!");
        Scanner sc=new Scanner(System.in);
        String target=sc.next();
        
        
        String encrypted=td.encrypt(target);
        System.out.println(" After Encrypting ...."+encrypted);
        String decrypted=td.decrypt(encrypted);

//        System.out.println("String To Encrypt: "+ target);
//        System.out.println("Encrypted String:" + encrypted);
        System.out.println("Decrypted String:" + decrypted);

    }


}



//next class


package reporting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javaCode.CommonFunctions;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import testScripts.driverScript;
import testScripts.generateTestNG;

public class CustomTestNGReporter implements IReporter {
	
	//This is the customize emailabel report template file path.
	private static final String emailableReportTemplateFile = generateTestNG.str_folderPath+"/src/test/java/reporting/customize-emailable-report-template.html";

	private static int count = 1;
	
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		
		try
		{
			// Get content data in TestNG report template file.
			String customReportTemplateStr = this.readEmailabelReportTemplate();
			
			// Create custom report title.
			//String customReportTitle = this.getCustomReportTitle("Custom TestNG Report");
			
			// Create test suite summary data.
			String customSuiteSummary = this.getTestSuiteSummary(suites);
			
			// Create test methods summary data.
			String customTestMethodSummary = this.getTestMehodSummary(suites);
			
			// Replace report title place holder with custom title.
			//customReportTemplateStr = customReportTemplateStr.replaceAll("\\$TestNG_Custom_Report_Title\\$", customReportTitle);
			
			// Replace test suite place holder with custom test suite summary.
			customReportTemplateStr = customReportTemplateStr.replaceAll("\\$Test_Case_Summary\\$", customSuiteSummary);
			
			// Replace test methods place holder with custom test method summary.
			customReportTemplateStr = customReportTemplateStr.replaceAll("\\$Test_Case_Detail\\$", customTestMethodSummary);
			
			// Write replaced test report content to custom-emailable-report.html.
			File targetFile = new File(outputDirectory + "/custom-emailable-report.html");
			FileWriter fw = new FileWriter(targetFile);
			fw.write(customReportTemplateStr);
			fw.flush();
			fw.close();
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/* Read template content. */
	private String readEmailabelReportTemplate()
	{
		StringBuffer retBuf = new StringBuffer();
		
		try {
		
			File file = new File(this.emailableReportTemplateFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			String line = br.readLine();
			while(line!=null)
			{
				retBuf.append(line);
				line = br.readLine();
			}
			
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}finally
		{
			return retBuf.toString();
		}
	}
	
	/* Build custom report title. */
	private String getCustomReportTitle(String title)
	{
		StringBuffer retBuf = new StringBuffer();
		retBuf.append(title + " " + this.getDateInStringFormat(new Date()));
		return retBuf.toString();
	}
	
	/* Build test suite summary data. */
	private String getTestSuiteSummary(List<ISuite> suites) throws IOException
	{
		
		 String str_TestDataFileName = generateTestNG.str_FILENAME;
		String str_TestDataSheetName =  "TestNameMapping";
		
		// Read data from TestSet and Pass -->  Test Name if Execute is Y; Env; Browser
		
		String testData_filePath = generateTestNG.dataFilePath;
		String fileName = str_TestDataFileName; //"HybridFrameWork_TestData.xlsx";
		String sheetName_TS = str_TestDataSheetName; //"TestSet";
		
		//Create an object of File class to open xlsx file
		File file_TS =    new File(testData_filePath+fileName);
		
		//Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = new FileInputStream(file_TS);
		Workbook myWorkBook = null;
		
		//Find the file extension by splitting file name in substring and getting only extension name
		String fileExtensionName = fileName.substring(fileName.indexOf("."));

		//Check condition if the file is xlsx file
		if(fileExtensionName.equals(".xlsx")){
		    //If it is xlsx file then create object of XSSFWorkbook class
			myWorkBook = new XSSFWorkbook(inputStream);
		}
		//Check condition if the file is xls file
		else if(fileExtensionName.equals(".xls")){
		    //If it is xls file then create object of XSSFWorkbook class
			myWorkBook = new HSSFWorkbook(inputStream);
		}				
		Sheet mySheet_TS = myWorkBook.getSheet(sheetName_TS);

		int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();

		int int_CellIndex_AutString = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "AutomationString");				
		int int_CellIndex_ReportingTestName = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "ReportingTestName");				
		
		
		StringBuffer retBuf = new StringBuffer();
		
		try
		{
			int totalTestCount = 0;
			int totalTestPassed = 0;
			int totalTestFailed = 0;
			int totalTestSkipped = 0;
			
			for(ISuite tempSuite: suites)
			{
				//retBuf.append("<tr><td colspan=11><center><b>" + tempSuite.getName() + "</b></center></td></tr>");
				
				Map<String, ISuiteResult> testResults = tempSuite.getResults();

				for (ISuiteResult result : testResults.values()) {
					retBuf.append("<tr>");
					String str_reportingTestName = "";
					ITestContext testObj = result.getTestContext();
					
					for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
					{
						String str_CellIndex_AutString = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_AutString).toString();
						System.err.println(str_CellIndex_AutString);
						if(str_CellIndex_AutString.contains(testObj.getName())){
							str_reportingTestName = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_ReportingTestName).toString();
						}
						
					}
					
					totalTestPassed = testObj.getPassedTests().getAllMethods().size();
					totalTestSkipped = testObj.getSkippedTests().getAllMethods().size();
					totalTestFailed = testObj.getFailedTests().getAllMethods().size();
					
					totalTestCount = totalTestPassed + totalTestSkipped + totalTestFailed;
					
					/* Test name. */
					retBuf.append("<td>");
					System.err.println(str_reportingTestName);
					retBuf.append(str_reportingTestName);
					retBuf.append("</td>");

/*					System.err.println("__________*********************__________");
					System.err.println(testObj.getName());
					System.err.println(driverScript.passedStepsMap);
					System.err.println(driverScript.passedStepsMap.get(testObj.getName()));
					System.err.println(testObj.getName());
					System.err.println("__________*********************__________");
*/
					/* Include groups. */
					retBuf.append("<td>");
					if(driverScript.passedStepsMap.containsKey(testObj.getName())){
						retBuf.append(driverScript.passedStepsMap.get(testObj.getName()));
					}
					retBuf.append("</td>");
					
					
					/* Exclude groups. */
					retBuf.append("<td><a href=\""+generateTestNG.str_cftLink+"target/test-output/html/suite1_test"+(count++)+"_results.html\">");
					if(driverScript.failedStepsMap.containsKey(testObj.getName())){
						retBuf.append(driverScript.failedStepsMap.get(testObj.getName()));
					}
					retBuf.append("</a></td>");
					/*if(driverScript.failedStepsMap.containsKey(testObj.getName())){
						x = driverScript.failedStepsMap.get(testObj.getName());
					}

					if(x==0){
						retBuf.append("<td>");
						retBuf.append(x);
						count++;
						retBuf.append("</td>");
					}else{
						retBuf.append("<td><a href=\"http://cft-devops.apps.ml.com/mercury/dev/job/QA/job/entitlements_automation/ws/target/test-output/html/suite1_test"+(count++)+"_results.html\">");
						retBuf.append(x);
						retBuf.append("</a></td>");
					}
						*/
					
/*					 Total method count. 
					retBuf.append("<td>");
					retBuf.append(totalTestCount);
					retBuf.append("</td>");
*/					
					/* Passed method count. */
					/*retBuf.append("<td bgcolor=green>");
					retBuf.append(totalTestPassed);
					retBuf.append("</td>");
					*/
/*					 Skipped method count. 
					retBuf.append("<td bgcolor=yellow>");
					retBuf.append(totalTestSkipped);
					retBuf.append("</td>");
*/					
					
					/* Failed method count. 
					retBuf.append("<td bgcolor=red>");
					retBuf.append(totalTestFailed);
					retBuf.append("</td>");
					*/
					/* Get browser type. */
					String browserType = tempSuite.getParameter("browserType");
					if(browserType==null || browserType.trim().length()==0)
					{
						try{
							browserType = testObj.getName().substring(0,testObj.getName().indexOf("#"));
						}catch(Exception e){
							browserType = "LOCAL";
						}
					}
					
					/* Append browser type. */
					retBuf.append("<td>");
					retBuf.append(browserType);
					retBuf.append("</td>");
					
					/* Start Date*/
					Date startDate = testObj.getStartDate();
					retBuf.append("<td>");
					retBuf.append(this.getDateInStringFormat(startDate));
					retBuf.append("</td>");
					
					/* End Date*/
					Date endDate = testObj.getEndDate();
					retBuf.append("<td>");
					retBuf.append(this.getDateInStringFormat(endDate));
					retBuf.append("</td>");
					
					/* Execute Time */
					long deltaTime = endDate.getTime() - startDate.getTime();
					String deltaTimeStr = this.convertDeltaTimeToString(deltaTime);
					retBuf.append("<td>");
					retBuf.append(deltaTimeStr);
					retBuf.append("</td>");
					
					retBuf.append("</tr>");
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}finally
		{
			return retBuf.toString();
		}
	}

	/* Get date string format value. */
	private String getDateInStringFormat(Date date)
	{
		StringBuffer retBuf = new StringBuffer();
		if(date==null)
		{
			date = new Date();
		}
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		retBuf.append(df.format(date));
		return retBuf.toString();
	}
	
	/* Convert long type deltaTime to string with format hh:mm:ss. */
	private String convertDeltaTimeToString(long deltaTime)
	{
		StringBuffer retBuf = new StringBuffer();
		
		long milli = deltaTime;
		
		long seconds = deltaTime / 1000;
		
		long minutes = seconds / 60;
		
		long hours = minutes / 60;
		
		retBuf.append(hours + ":" + minutes + ":" + seconds + ":" + milli);
		
		return retBuf.toString();
	}
	
	/* Get test method summary info. */
	private String getTestMehodSummary(List<ISuite> suites)
	{
		StringBuffer retBuf = new StringBuffer();
		
		try
		{
			for(ISuite tempSuite: suites)
			{
				retBuf.append("<tr><td colspan=7><center><b>" + tempSuite.getName() + "</b></center></td></tr>");
				
				Map<String, ISuiteResult> testResults = tempSuite.getResults();
				
				for (ISuiteResult result : testResults.values()) {
					
					ITestContext testObj = result.getTestContext();

					String testName = testObj.getName();
					
					/* Get failed test method related data. */
					IResultMap testFailedResult = testObj.getFailedTests();
					String failedTestMethodInfo = this.getTestMethodReport(testName, testFailedResult, false, false);
					retBuf.append(failedTestMethodInfo);
					
					/* Get skipped test method related data. */
					IResultMap testSkippedResult = testObj.getSkippedTests();
					String skippedTestMethodInfo = this.getTestMethodReport(testName, testSkippedResult, false, true);
					retBuf.append(skippedTestMethodInfo);
					
					/* Get passed test method related data. */
					IResultMap testPassedResult = testObj.getPassedTests();
					String passedTestMethodInfo = this.getTestMethodReport(testName, testPassedResult, true, false);
					retBuf.append(passedTestMethodInfo);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}finally
		{
			return retBuf.toString();
		}
	}
	
	/* Get failed, passed or skipped test methods report. */
	private String getTestMethodReport(String testName, IResultMap testResultMap, boolean passedReault, boolean skippedResult)
	{
		StringBuffer retStrBuf = new StringBuffer();
		
		String resultTitle = testName;
		
		String color = "green";
		
		if(skippedResult)
		{
			resultTitle += " - Skipped ";
			color = "yellow";
		}else
		{
			if(!passedReault)
			{
				resultTitle += " - Failed ";
				color = "red";
			}else
			{
				resultTitle += " - Passed ";
				color = "green";
			}
		}
		
		retStrBuf.append("<tr bgcolor=" + color + "><td colspan=7><center><b>" + resultTitle + "</b></center></td></tr>");
			
		Set<ITestResult> testResultSet = testResultMap.getAllResults();
			
		for(ITestResult testResult : testResultSet)
		{
			String testClassName = "";
			String testMethodName = "";
			String startDateStr = "";
			String executeTimeStr = "";
			String paramStr = "";
			String reporterMessage = "";
			String exceptionMessage = "";
			
			//Get testClassName
			testClassName = testResult.getTestClass().getName();
				
			//Get testMethodName
			testMethodName = testResult.getMethod().getMethodName();
				
			//Get startDateStr
			long startTimeMillis = testResult.getStartMillis();
			startDateStr = this.getDateInStringFormat(new Date(startTimeMillis));
				
			//Get Execute time.
			long deltaMillis = testResult.getEndMillis() - testResult.getStartMillis();
			executeTimeStr = this.convertDeltaTimeToString(deltaMillis);
				
			//Get parameter list.
			Object paramObjArr[] = testResult.getParameters();
			for(Object paramObj : paramObjArr)
			{
				paramStr += (String)paramObj;
				paramStr += " ";
			}
				
			//Get reporter message list.
			List<String> repoterMessageList = Reporter.getOutput(testResult);
			for(String tmpMsg : repoterMessageList)				
			{
				reporterMessage += tmpMsg;
				reporterMessage += " ";
			}
				
			//Get exception message.
			Throwable exception = testResult.getThrowable();
			if(exception!=null)
			{
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				exception.printStackTrace(pw);
				
				exceptionMessage = sw.toString();
			}
			
			retStrBuf.append("<tr bgcolor=" + color + ">");
			
			/* Add test class name. */
			retStrBuf.append("<td>");
			retStrBuf.append(testClassName);
			retStrBuf.append("</td>");
			
			/* Add test method name. */
			retStrBuf.append("<td>");
			retStrBuf.append(testMethodName);
			retStrBuf.append("</td>");
			
			/* Add start time. */
			retStrBuf.append("<td>");
			retStrBuf.append(startDateStr);
			retStrBuf.append("</td>");
			
			/* Add execution time. */
			retStrBuf.append("<td>");
			retStrBuf.append(executeTimeStr);
			retStrBuf.append("</td>");
			
			/* Add parameter. */
			retStrBuf.append("<td>");
			retStrBuf.append(paramStr);
			retStrBuf.append("</td>");
			
			/* Add reporter message. */
			retStrBuf.append("<td>");
			retStrBuf.append(reporterMessage);
			retStrBuf.append("</td>");
			
			/* Add exception message. */
			retStrBuf.append("<td>");
			retStrBuf.append(exceptionMessage);
			retStrBuf.append("</td>");
			
			retStrBuf.append("</tr>");

		}
		
		return retStrBuf.toString();
	}
	
	/* Convert a string array elements to a string. */
	private String stringArrayToString(String strArr[])
	{
		StringBuffer retStrBuf = new StringBuffer();
		if(strArr!=null)
		{
			for(String str : strArr)
			{
				retStrBuf.append(str);
				retStrBuf.append(" ");
			}
		}
		return retStrBuf.toString();
	}

}


//nextclass

package reporting;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javaCode.CommonFunctions;

import org.apache.poi.common.usermodel.Hyperlink;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import testScripts.driverScript;
import testScripts.generateTestNG;

	/*GUIDE TO USE THE BELOW EXCEL REPORTING FUNCTIONS
	______________________________________________________________________________________________________________________  
	FAILED REPORTS:
	
	//////////////////////////Reporting//////////////////////////
	String str_fileName = "testFunction";
	String str_screenShotFileName = CommonFunctions.takeScreenShotMethod(driver, str_fileName);
	Report.failDetailedReporting(driver, str_screenShotFileName, "Add as many columns as needed!");
	Report.Log_ReportNG("FAIL", "Search box is not open!", driver, str_screenShotFileName);
	System.err.println("Search box is not open!");
	/////////////////////////////////////////////////////////////
	______________________________________________________________________________________________________________________
	PASSED REPORTS:
	
	/////////////////////Reporting/////////////////////////
	Report.Log_ReportNG("PASS", "", str_pageName, "Test title", "Test description", "Pass", "Script", str_captureFailureMessage);
	Report.passDetailedReporting(str_pageName, "Test title", "Test description", "PASS", "", str_captureFailureMessage);
	System.out.println(str_pageName+ " : Test title : Test description : PASS ");
	//////////////////////////////////////////////////////		
	*/

public class Report
{

	public static void passDetailedReporting(String... args) throws IOException, InvalidFormatException {
		
		String file = driverScript.reportFileName;
		
		FileInputStream reportInputStream = new FileInputStream(file);
		Workbook reportWorkBook = WorkbookFactory.create(reportInputStream);

		Sheet reportSpreadSheet = reportWorkBook.getSheetAt(0);
		
		int int_rowCount = reportSpreadSheet.getLastRowNum();
		Row row = reportSpreadSheet.createRow(++int_rowCount);
		
		//Style
		CellStyle style = reportWorkBook.createCellStyle();
		Font font = reportWorkBook.createFont();
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.RED.index);
		font.setColor(HSSFColor.RED.index);
		
		List<String> str_dataList = new ArrayList<String>();

		for (String i : args)
			str_dataList.add(i);

		for(int i=0;i<str_dataList.size();i++){
			
			Cell cell = row.createCell(i);
			cell.setCellValue(str_dataList.get(i));
					
		}

		reportInputStream.close();
		FileOutputStream outputStream = new FileOutputStream(file);
		reportWorkBook.write(outputStream);
		outputStream.close();
	}
	
	public static void failDetailedReporting(WebDriver driver, String str_screenShotFileName, String... args) throws IOException, InvalidFormatException {

		String file = driverScript.reportFileName;
				
		FileInputStream reportInputStream = new FileInputStream(file);
		Workbook reportWorkBook = WorkbookFactory.create(reportInputStream);

		Sheet reportSpreadSheet = reportWorkBook.getSheetAt(0);
		
		int int_rowCount = reportSpreadSheet.getLastRowNum();
		Row row = reportSpreadSheet.createRow(++int_rowCount);
		
		//Style
		CellStyle style = reportWorkBook.createCellStyle();
		Font font = reportWorkBook.createFont();
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.RED.index);
		font.setColor(HSSFColor.RED.index);
		
		List<String> str_dataList = new ArrayList<String>();

		for (String i : args)
			str_dataList.add(i);

		for(int i=0;i<str_dataList.size();i++){
	
			//row = spreadSheet.createRow(++int_rowCount);
				
			//Cell 1
			Cell cell = row.createCell(i);
	
			if(i==2){
			//Cell 5
				cell.setCellValue(str_dataList.get(i));
				row.getCell(2).setCellStyle(style);
			}else if(i==2 || i==3  || i==4 ){
				//Cell 5
				cell.setCellValue(str_dataList.get(i));
				CreationHelper createHelper21 = reportWorkBook.getCreationHelper();
				Hyperlink link21 = createHelper21.createHyperlink(Hyperlink.LINK_FILE);
				String screenShotURL = generateTestNG.str_folderPath + "ScreenShots/"+CommonFunctions.getTimeStamp("yyyyMMdd") +"/"+str_screenShotFileName;
				String screenShotCorrected = CommonFunctions.convertToFileURL(screenShotURL);
				link21.setAddress(screenShotCorrected);
				cell.setHyperlink((org.apache.poi.ss.usermodel.Hyperlink) link21);
				row.getCell(i).setCellStyle(style);
				
			}else{
				cell.setCellValue(str_dataList.get(i));
				
			}
		}
		
		reportInputStream.close();
		FileOutputStream outputStream = new FileOutputStream(file);
		reportWorkBook.write(outputStream);
		outputStream.close();
	}
	
	public static void Log_ReportNG(String str_status,
			String str_ScnSht_Folder, String... args) throws IOException {

		if(str_status.equals("PASS")){
			Reporter.log("<br> <font color=green> "+ args[0] +" : "+ args[1] +" : "+ args[2] +" </font> </br>");
		}else if(str_status.equals("FAIL")){
	
			String screenShotURL = generateTestNG.str_folderPath + "ScreenShots/"+CommonFunctions.getTimeStamp("yyyyMMdd") +"/"+str_ScnSht_Folder;
			Reporter.log("<br> <font color=red> <a href=\""+screenShotURL+"\""+ args[0] +" : "+ args[1] +" : "+ args[2]+" : "+ args[3] +"</a> </font>");
			//Reporter.log("<font color=red> "+ args[0] +" : "+ args[1] +" : "+ args[2] + " </font> <br>");
		}
	}
}




//customize emailable report

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=gbk" />
    <title>TestNG Report</title>
    <style type="text/css">table {margin-bottom:10px;border-collapse:collapse;empty-cells:show}th,td {border:1px solid #009;padding:.25em .5em}th {vertical-align:bottom}td {vertical-align:top}table a {font-weight:bold}.stripe td {background-color: #E6EBF9}.num {text-align:right}.passedodd td {background-color: #3F3}.passedeven td {background-color: #0A0}.skippedodd td {background-color: #DDD}.skippedeven td {background-color: #CCC}.failedodd td,.attn {background-color: #F33}.failedeven td,.stripe .attn {background-color: #D00}.stacktrace {white-space:pre;font-family:monospace}.totop {font-size:85%;text-align:center;border-bottom:2px solid #000}</style>
  </head>
  <body>
    <table>
      <thead>
        <tr>
        
          <th>Test Name</th>
           <th>Steps Passed</th>
          <th>Steps Failed</th>
          <th>Browser</th>
          <th>Start Time</th>
          <th>End Time</th>
          <th>Execute Time (mm:hh:ss:ms)</th>
        </tr>
       </thead> 
       $Test_Case_Summary$
    </table>
    
  </body>
</html>



//package testScripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import javaCode.CommonFunctions;
import javaCode.GLISCommFuncs;
import reporting.Report;
import objectRepository.ObjFunctions;

public class driverScript
{

	public static String fileName = "GLIS2_TestData.xlsx";
	public static String screenShotFolder = "";
	public static String reportsFolder = "";
	
	private static int sauceFlag = 0;
	private int int_Iter_Global = 0;

	public static Map<String, Integer> failedStepsMap = generateTestNG.failedStepsMap;
	public static Map<String, Integer> passedStepsMap = generateTestNG.passedStepsMap;
	
	WebDriver driver;

	public static String str_SauceAccount = "mohsina";

	public static String reportFileName = "";
	
	@BeforeSuite
	public void beforeSuite() throws IOException, InterruptedException{

		try{
			Thread.sleep(1000);
			screenShotFolder = CommonFunctions.createAFolderInProjectDir(generateTestNG.str_folderPath,"ScreenShots") + "/"+CommonFunctions.getTimeStamp("yyyyMMdd")+"/";
			Thread.sleep(1000);
			reportsFolder = CommonFunctions.createAFolderInProjectDir(generateTestNG.str_folderPath,"Reports") + "/"+CommonFunctions.getTimeStamp("yyyyMMdd")+"/";
			CommonFunctions.createAFolderInProjectDir(generateTestNG.str_folderPath+"ScreenShots/",CommonFunctions.getTimeStamp("yyyyMMdd"));
			CommonFunctions.createAFolderInProjectDir(generateTestNG.str_folderPath+"Reports/",CommonFunctions.getTimeStamp("yyyyMMdd"));
			Thread.sleep(1000);
		}catch(Exception e){
			System.err.println("Severe error occured while initializing reports, please call POC. Testing cannot proceed!");
			Assert.assertFalse(true);
		}
	}	

	@Parameters({ "str_Iter_Global" })
    @Test
    public void myTest(String param) throws IOException
    {
    	CommonFunctions cf = new CommonFunctions();

    	int int_executeHighLevelFlag = 0;

		System.setProperty("org.uncommons.reportng.escape-output", "false");
    	
		/*long int_Current_ThreadID = Thread.currentThread().getId();
		long int_ThreadIndex = 0;
		System.out.println("Thread Index before: " + int_Current_ThreadID );
		//result = testStatement ? value1 : value2;
		int_ThreadIndex = int_Current_ThreadID >= 8 ? int_Current_ThreadID - 8 : int_Current_ThreadID - 1;
		
		System.out.println("Thread Index after: " + int_ThreadIndex);    	
    	
		int_Iter_Global = (int) int_ThreadIndex;*/


		int_Iter_Global = Integer.parseInt(param);
		
		ArrayList<String> arrayList_BrowserTypes = new ArrayList<String>();
    	ArrayList<String> arrayList_BrowserTypes_LOCAL = new ArrayList<String>();
    	
		try {
			String str_TestDataFileName = fileName;
			String str_TestDataSheetName =  "TestSet";
			
			String testData_filePath = generateTestNG.dataFilePath;		
			String fileName = str_TestDataFileName; //"HybridFrameWork_TestData.xlsx";
			String sheetName_TS = str_TestDataSheetName; //"TestSet";
			
			File file_TS =    new File(generateTestNG.dataFilePath+fileName);
			//Testing...
			FileInputStream inputStream = new FileInputStream(file_TS);
			//result = testStatement ? value1 : value2;
			Workbook myWorkBook = fileName.substring(fileName.indexOf(".")).equals(".xlsx") ? new XSSFWorkbook(inputStream) 
								: fileName.substring(fileName.indexOf(".")).equals(".xlsx") ? new HSSFWorkbook(inputStream)
								: null;
							
			Sheet mySheet_TS = myWorkBook.getSheet(sheetName_TS);

			int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();
			
			System.err.println("Row Count from TestSet: "+ rowCount_TS);
						
			int int_CellIndex_UserType = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "User Type");
			int int_CellIndex_Execute = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Execute");
			int int_CellIndex_Execute_HL = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Execute_HL");
			int int_CellIndex_TestName = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Test Name");
			int int_CellIndex_UserId = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "UserID");
			int int_CellIndex_Pwd = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Pwd");
			int int_CellIndex_Environment = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Environment");
			int int_CellIndex_IE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "IE");
			int int_CellIndex_Chrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Chrome");
			int int_CellIndex_SauceIE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_IE");
			int int_CellIndex_SauceChrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_CHROME");
			String str_Environment = null;
			
			for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
			{
				System.out.println(iRow_TS);

				String str_Execute_TS = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Execute).toString(); 

				String str_Execute_HL_TS = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Execute_HL).toString();

				if(str_Execute_TS.trim().equalsIgnoreCase("Y")){
					//result = testStatement ? value1 : value2;
					int_executeHighLevelFlag = str_Execute_HL_TS.trim().equalsIgnoreCase("Y") ? 1
											 : str_Execute_HL_TS.trim().equalsIgnoreCase("N") ? 0
											 : null;
					
					String str_UserType	= mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserType).toString();
					String str_TestName = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_TestName).toString();
					String str_UserID = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserId).toString();
					String str_Pwd = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Pwd).toString();
					str_Environment = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Environment).toString();
					
					String str_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_IE).toString();    		
					String str_Chrome = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Chrome).toString();
					
					String str_SAUCE_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceIE).toString();
					String str_SAUCE_CHROME = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceChrome).toString();
					
					if (str_IE.equalsIgnoreCase("Y"))
					{
						if(!arrayList_BrowserTypes.contains("LOCAL")) 
						{
							arrayList_BrowserTypes.add("LOCAL");	
						}
						arrayList_BrowserTypes_LOCAL.add("IE"+"#"+str_UserID +"#" + str_Pwd + "#" + str_TestName + "#" + str_UserType);
					}
					if (str_Chrome.equalsIgnoreCase("Y"))
					{
						if(!arrayList_BrowserTypes.contains("LOCAL")) 
						{
							arrayList_BrowserTypes.add("LOCAL");	
						}
						arrayList_BrowserTypes_LOCAL.add("CHROME"+"#"+str_UserID +"#" + str_Pwd+ "#" + str_TestName+ "#" + str_UserType);
					}
					
					if (!str_SAUCE_IE.equalsIgnoreCase("N"))
					{
						arrayList_BrowserTypes.add("SAUCE_IE" + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType + "#" + str_SAUCE_IE);
					}
						
					if (!str_SAUCE_CHROME.equalsIgnoreCase("N"))
					{
						arrayList_BrowserTypes.add("SAUCE_CHROME"  + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType+ "#" + str_SAUCE_CHROME);
					}
			
				} //if (str_Execute_TS.trim().equalsIgnoreCase("Y"))
				
			} //for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)			
			
			System.out.println(int_Iter_Global  + " > Data from Array: " + arrayList_BrowserTypes.get(int_Iter_Global));
			//System.out.println("LOCAL ARRAY..." + arrayList_BrowserTypes_LOCAL);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Total Combinations Selected...: (driverScript)" + arrayList_BrowserTypes.size() + "   ---   "+arrayList_BrowserTypes);

			String str_UserData  = "";
			int x = 0;
			for(int i=0;i<generateTestNG.arrayList_BrowserTypes.size();i++){
				if(generateTestNG.arrayList_BrowserTypes.get(i).equals(arrayList_BrowserTypes.get(int_Iter_Global))){
					str_UserData = generateTestNG.arrayList_BrowserTypes.get(int_Iter_Global);
				}
			}
			
			//String str_UserData = generateTestNG.arrayList_BrowserTypes.get(int_Iter_Global);
			passedStepsMap.put(str_UserData, 0);
			failedStepsMap.put(str_UserData, 0);
			
			String[] arr_UserData = str_UserData.split("#"); //"SAUCE_IE" + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType + "#" + str_SAUCE_IE
			
			String str_BrowserType = arr_UserData[0]; //SAUCE_IE
			
			String str_URL 
					= str_Environment.equalsIgnoreCase("PP") ? "https://markets-pp.ml.com/" 
					: str_Environment.equalsIgnoreCase("PROD") ? "https://markets.ml.com/"
					: "";
			reportFileName = cf.CreateDetailedFile(str_Environment, reportsFolder, "GLIS2_"+CommonFunctions.getTimeStamp("yyyyMMdd_HHmm")+"_"+CommonFunctions.randomString(6)+".xlsx");

			String str_UserName = null;
			String str_Pwd = null;			
			String str_TestName = null;
					
			if(str_BrowserType.startsWith("SAUCE_")) 
			{
				sauceFlag = 1;

				str_UserName = arr_UserData[1];
				str_Pwd = arr_UserData[2];
				str_TestName = arr_UserData[3];  
				String str_TestName_N_BrowserType_N_UserName = str_TestName + "#" + str_BrowserType + "#" + str_UserName;
				
				String str_PlatformNBrowserVersion = arr_UserData[5]; //Windows 7;11.0
				String str_Platform = str_PlatformNBrowserVersion.split(";")[0];
				String str_Version = str_PlatformNBrowserVersion.split(";")[1];
				String str_deviceName = null;
				String str_deviceOrientation = null;
				
				String str_UserType = arr_UserData[4];
				
				String str_BrowserType2 = str_BrowserType.split("_")[1];
				driver = CommonFunctions.openBrowser_SAUCE(str_SauceAccount,
						str_BrowserType2, str_Version, str_Platform, str_URL,
						str_deviceName, str_deviceOrientation,
						str_TestName_N_BrowserType_N_UserName, str_UserName);
				
				CommonFunctions.Login(driver, str_UserName, str_Pwd, str_URL, str_UserType);
				String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms").format(new Date());
				Reporter.log("<br> <font color=green>"+ "TimeStamp: [" + timeStamp + "]" + "</font> </br>");	
				
				CommonFunctions.waitUntil_jQueryActiveNReadyStateComplete(driver, 120000);
				
				System.out.println(str_TestName + "; Title: " + driver.getTitle());
				//result = testStatement ? value1 : value2;
				int_executeHighLevelFlag = !(arrayList_BrowserTypes.get(int_Iter_Global).contains("High Level")) ? 0 : int_executeHighLevelFlag;
				
				execute_DataSheet(driver, str_UserData, str_BrowserType, str_UserType, myWorkBook, str_TestName, str_UserType,
						str_TestDataFileName, testData_filePath, str_URL, sauceFlag, int_executeHighLevelFlag,
						str_UserName, str_Pwd, reportFileName);

				CommonFunctions.Logout(driver, str_UserName);
				
				if (driver != null) 
				{
					driver.close();
					driver.quit();
					driver = null;				
				}
				
			} //if(str_BrowserType.startsWith("SAUCE_"))

			else if (str_BrowserType.startsWith("LOCAL"))
			{
				
				System.out.println("FROM LOCAL IF CONDITION,,,,,,,");
				
				System.out.println("LOCAL DATA INSIDE IF CONDITION: " + arrayList_BrowserTypes_LOCAL);
			
				for (String str_UserAndBrowserData : arrayList_BrowserTypes_LOCAL) 
				{
					
					System.out.println("INSIDE FOR LOOP....data: " + str_UserAndBrowserData);
					str_BrowserType = str_UserAndBrowserData.split("#")[0];
					str_UserName = str_UserAndBrowserData.split("#")[1];
					str_Pwd = str_UserAndBrowserData.split("#")[2];
					str_TestName = str_UserAndBrowserData.split("#")[3];
					
					String str_UserType = str_UserAndBrowserData.split("#")[4];
					
					System.out.println("str_BrowserType: " + str_BrowserType + "; str_UserName: " + str_UserName + "; str_TestName: "+ str_TestName);
					
					driver = CommonFunctions.openBrowser(str_BrowserType, str_URL);

					CommonFunctions.Login(driver, str_UserName,
							str_Pwd, str_URL, str_UserType);
					
					String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms").format(new Date());
					Reporter.log("<br> <font color=green>"+ "TimeStamp: [" + timeStamp + "]" + "</font> </br>");	
					CommonFunctions.waitUntil_jQueryActiveNReadyStateComplete(driver, 120000);
					
					System.out.println(str_TestName + "; Title: " + driver.getTitle());
					
					//String str_BrowserType_N_UserName = str_BrowserType+ "_" + str_UserName;
					//String str_TimeStamp = CommonFunctions.getTimeStamp("yyyy-MM-dd-HH-mm-ss");
					//String reportLocation = str_ReportFolderPath + "\\"+ str_BrowserType_N_UserName + "_" + str_UserType +"_Report_["+ str_TimeStamp + "].html";
					execute_DataSheet(driver, str_UserData, str_BrowserType, str_UserType, myWorkBook, str_TestName,
							str_UserType, str_TestDataFileName,
							testData_filePath, str_URL,
							sauceFlag, int_executeHighLevelFlag, str_UserName, str_Pwd, reportFileName);

					Thread.sleep(2000);
					CommonFunctions.Logout(driver, str_UserName);
					
					if (driver != null) 
					{
						driver.close();
						driver.quit();
						driver = null;				
					}
				}
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute_DataSheet(WebDriver driver, String str_UserData, String str_browser, String str_UserType, Workbook myWorkBook,
			String str_TestName, String str_Enviroment, String str_TestDataFileName, String testData_filePath,
			String str_URL, int sauceFlag, int executeHighLevelFlag, String str_UserName, String str_Pwd,
			String reportFileName) throws IOException, InvalidFormatException, InterruptedException {

		ObjFunctions obf = new ObjFunctions();
		
		System.out.println("**********" + executeHighLevelFlag + "//" + "**************");
	    
		String str_sheetName_TD = "TestData";
		String str_TestCaseName = str_TestName; //"Historical Close Comparison";

	    Sheet mySheet_TD = myWorkBook.getSheet(str_sheetName_TD);
		int int_rowCountDataSheet = mySheet_TD.getLastRowNum()-mySheet_TD.getFirstRowNum();
	    
	    System.out.println("Row Count in Test Data: "+ int_rowCountDataSheet);

	    int int_StartRow = 0;
	    int int_EndRow = 0;
	    
	    if(!(executeHighLevelFlag==1)){
		    for (int i=0; i < int_rowCountDataSheet+1; i++) {
		    	if (mySheet_TD.getRow(i).getCell(1).toString().equalsIgnoreCase(str_TestCaseName)){
		    		int_StartRow = i;
		    		break;
		    	}
		    }
		    for (int j=int_rowCountDataSheet; j>=0; j--) {
		    	if (mySheet_TD.getRow(j).getCell(1).toString().equalsIgnoreCase(str_TestCaseName)){
		    		int_EndRow = j;
		    		break;
		    	}
		    }
	    }else{
		
	    	int_StartRow = 2;
	    	int_EndRow = int_rowCountDataSheet;
	    }
	    System.out.println("Start Row: "+ int_StartRow + " End Row: "+ int_EndRow);
	    int int_CellIndex_UserType_TD = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, "TestData", testData_filePath, 0, "Exe_" + str_Enviroment);
	    
	    String str_ExecutionFlag = null;							    
	    
	    if(executeHighLevelFlag==1){

		}else if(executeHighLevelFlag==0){
			for (int iRow= int_StartRow+1; iRow<=int_EndRow; iRow=iRow+2){
		    	 
		    	str_ExecutionFlag = mySheet_TD.getRow(iRow).getCell(int_CellIndex_UserType_TD).toString().trim();
		    	
		    	if (str_ExecutionFlag.contains("Y") && str_ExecutionFlag!="")
		    	{

		    		int int_KeywordColNum = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, "TestData", testData_filePath, 0, "Business Component");
	
		    		String str_Keyword = mySheet_TD.getRow(iRow).getCell(int_KeywordColNum).toString();
		    		
		    		int int_TotalCols = mySheet_TD.getRow(iRow).getLastCellNum();
	
		    		ArrayList<String> arrayList_ParametersData = new ArrayList<String>();
		    		
			    	for (int iCol = int_KeywordColNum+1 ;iCol<int_TotalCols; iCol++ ){
			    		
			    		String str_CellValue = mySheet_TD.getRow(iRow).getCell(iCol).toString();

			    		if (!str_CellValue.isEmpty())	
			    		{
			    			arrayList_ParametersData.add(str_CellValue);
			    		}
			    	}
			    	
			    	String [] arrDataFromSheet = arrayList_ParametersData.size() > 0 ? CommonFunctions.getArrayListItemsAsArray(arrayList_ParametersData) : null;
		    	 
					try {

						if(str_Keyword.equals("Test")){
							GLISCommFuncs.test(driver, str_UserData, str_URL, arrDataFromSheet[0]);
						}else if(str_Keyword.equals("Navigate_Direct")){
						   
						}
						
					} catch (Exception e) {
						//////////////////////////Reporting//////////////////////////
						String str_fileName = "driverScript";
						String str_screenShotFileName = CommonFunctions.takeScreenShotMethod(driver, str_fileName);
						System.err.println("Exception occurred: "+e.getMessage());
						//Report.Log_ReportNG("FAIL", str_screenShotFileName, "Exception occurred: "+e.getMessage());
						/////////////////////////////////////////////////////////////
						e.printStackTrace();
					}
				} // if (str_BusComp.equalsIgnoreCase("Y") && str_BusComp!="")
			} // for (int iRow= int_StartRow+1; iRow<int_EndRow; iRow=iRow+2)
		}
    }

	@AfterSuite
	public void createSummary() throws IOException{

	}
	
}



package testScripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javaCode.CommonFunctions;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class generateTestNG {

	public static String str_folderPath = System.getProperty("user.dir") + "/";

	public static Map<String, Integer> failedStepsMap = new ConcurrentHashMap<String, Integer>();
	public static Map<String, Integer> passedStepsMap = new ConcurrentHashMap<String, Integer>();

	public static String dataFilePath = "";
	
	public static ArrayList<String> arrayList_BrowserTypes = new ArrayList<String>();
	//public static String str_cftLink = "http://cft-devops.apps.ml.com/mercury/dev/job/QA/job/entitlements_automation/ws/";
	public static String str_cftLink = "file:///C:/Working/Workspace/Glis2Automation/";

	public static String str_FILENAME = "GLIS2_TestData.xlsx";
	
	@Test
	public static void main() throws IOException 
	{
		
		if(str_folderPath.contains("target")){
			str_folderPath = str_folderPath.replace("target", "");
		}

		dataFilePath = str_folderPath + "src/test/java/testData/";
		
		int mb = 1024*1024;
		
		//Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();
		
//		System.out.println("##### Heap utilization statistics [MB] #####");
//		
//		//Print used memory
//		System.out.println("Used Memory:" 
//			+ (runtime.totalMemory() - runtime.freeMemory()) / mb);
//
//		//Print free memory
//		System.out.println("Free Memory:" 
//			+ runtime.freeMemory() / mb);
//		
//		//Print total available memory
//		System.out.println("Total Memory:" + runtime.totalMemory() / mb);
//
//		//Print Maximum available memory
//		System.out.println("Max Memory:" + runtime.maxMemory() / mb);
	
		generateTestNG testNG1 = new generateTestNG();
		
		Map<String, String> testngParams = new HashMap<String, String>();
		
		testNG1.genTestNG(testngParams);
	
	}
	
	
	public static void genTestNG(Map<String,String> testngParams) throws IOException
	{
		ArrayList<String> arrayList_BrowserTypes2 = new ArrayList<String>();
		boolean bln_DataIssue = false;
		
		try {
			
			//Create an instance on TestNG
			 TestNG myTestNG = new TestNG();
			 
			//Create an instance of XML Suite and assign a name for it.
			 XmlSuite mySuite = new XmlSuite();
			 mySuite.setName("MySuite");
			 mySuite.addListener("org.uncommons.reportng.HTMLReporter");
			 mySuite.addListener("reporting.CustomTestNGReporter");
			 mySuite.addListener("org.uncommons.reportng.JUnitXMLReporter");		 
			 mySuite.setParallel("tests");
			 mySuite.setPreserveOrder("true");
			 mySuite.setThreadCount(50);
			 
			 String str_TestDataFileName = str_FILENAME;
			String str_TestDataSheetName =  "TestSet";
			
			// Read data from TestSet and Pass -->  Test Name if Execute is Y; Env; Browser
			
			String testData_filePath = dataFilePath;
			String fileName = str_TestDataFileName; //"HybridFrameWork_TestData.xlsx";
			String sheetName_TS = str_TestDataSheetName; //"TestSet";
			
			//Create an object of File class to open xlsx file
			File file_TS =    new File(testData_filePath+fileName);
			
			//Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file_TS);
			Workbook myWorkBook = null;
			
			//Find the file extension by splitting file name in substring and getting only extension name
			String fileExtensionName = fileName.substring(fileName.indexOf("."));

			//Check condition if the file is xlsx file
			if(fileExtensionName.equals(".xlsx")){
			    //If it is xlsx file then create object of XSSFWorkbook class
				myWorkBook = new XSSFWorkbook(inputStream);
			}
			//Check condition if the file is xls file
			else if(fileExtensionName.equals(".xls")){
			    //If it is xls file then create object of XSSFWorkbook class
				myWorkBook = new HSSFWorkbook(inputStream);
			}				
					

			//Read sheet inside the workbook by its name
			Sheet mySheet_TS = myWorkBook.getSheet(sheetName_TS);

			//Find number of rows in excel file
			int rowCount_TS = mySheet_TS.getLastRowNum()- mySheet_TS.getFirstRowNum();
			
			System.out.println("Row Count from TestSet: "+ rowCount_TS);
			//Print(mySheet.getPhysicalNumberOfRows());
			//String cellData = mySheet.getRow(1).getCell(1).toString();
			//Print("Cell Data: "+ cellData);
			//Print(mySheet.getRow(24).getCell(1).toString());
			
			//Get the range of a given Test Name(First and Last Row)

			//String str_TestCaseName = "Swaption Closing Skew";	    

			int int_CellIndex_UserType = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "User Type");
			int int_CellIndex_Execute = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Execute");
			int int_CellIndex_TestName = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Test Name");
			int int_CellIndex_UserId = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "UserID");
			int int_CellIndex_Pwd = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Pwd");
			int int_CellIndex_IE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "IE");
			int int_CellIndex_Chrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "Chrome");
			int int_CellIndex_SauceIE = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_IE");
			int int_CellIndex_SauceChrome = CommonFunctions.getCellIndexOfARow(str_TestDataFileName, str_TestDataSheetName, testData_filePath, 0, "SAUCE_CHROME");				
			
			for (int iRow_TS=1;iRow_TS<=rowCount_TS-1;iRow_TS++)
			{
				
				//Read Test Name and check if Execute is Y or N
				String str_Execute_TS = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Execute).toString();
				
				if (str_Execute_TS.trim().equalsIgnoreCase("Y"))
				{
					//Get Environment, IE, Chrome, FF and Sauce Browsers

					String str_TestName = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_TestName).toString();
					String str_UserID = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserId).toString();
					String str_UserType = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_UserType).toString();
					String str_Pwd = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Pwd).toString();
					//String str_Enviroment = mySheet_TS.getRow(iRow_TS).getCell(5).toString(); 
					String str_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_IE).toString();    		
					String str_Chrome = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_Chrome).toString();
					
					String str_SAUCE_IE = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceIE).toString();
					String str_SAUCE_CHROME = mySheet_TS.getRow(iRow_TS).getCell(int_CellIndex_SauceChrome).toString();
					
					//Build an Array with Browser Types that needs to be executed
					
					//if (str_IE.equalsIgnoreCase("Y") && str_SAUCE_IE.equalsIgnoreCase("N") && str_SAUCE_CHROME.equalsIgnoreCase("N")) 
					//str_Chrome.equalsIgnoreCase("Y") && str_SAUCE_IE.equalsIgnoreCase("N") && str_SAUCE_CHROME.equalsIgnoreCase("N"))
					
					if (str_IE.equalsIgnoreCase("Y")) 
					{
						if(!arrayList_BrowserTypes.contains("LOCAL"))
						{
							arrayList_BrowserTypes.add("LOCAL");
						}
					}
					
					
					if (str_Chrome.equalsIgnoreCase("Y"))
					//if (str_Chrome.equalsIgnoreCase("Y") && str_SAUCE_IE.equalsIgnoreCase("N") && str_SAUCE_CHROME.equalsIgnoreCase("N"))
					{
						if(!arrayList_BrowserTypes.contains("LOCAL"))
						{
							arrayList_BrowserTypes.add("LOCAL");
						}
					}
					
					//if (str_IE.equalsIgnoreCase("N") && str_Chrome.equalsIgnoreCase("N") && !str_SAUCE_IE.equalsIgnoreCase("N"))
					if (!str_SAUCE_IE.equalsIgnoreCase("N"))							
					{
						//arrayList_BrowserTypes.add(str_TestName + "#" +str_UserID + "#SAUCE_IE" + "#" + str_SAUCE_IE);
						arrayList_BrowserTypes.add("SAUCE_IE"  + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType+ "#" + str_SAUCE_IE);
						//arrayList_BrowserTypes.add(str_TestName + "#" +str_UserID + "#SAUCE_IE" + "#" + str_SAUCE_IE);
					}
						
					//if (str_IE.equalsIgnoreCase("N") && str_Chrome.equalsIgnoreCase("N") && !str_SAUCE_CHROME.equalsIgnoreCase("N"))
					if (!str_SAUCE_CHROME.equalsIgnoreCase("N"))							
					{
						//arrayList_BrowserTypes.add(str_TestName + "#" +str_UserID + "#SAUCE_CHROME"  + "#" + str_SAUCE_CHROME);
						arrayList_BrowserTypes.add("SAUCE_CHROME"  + "#" +str_UserID +"#" + str_Pwd + "#" + str_TestName+ "#" + str_UserType+ "#" + str_SAUCE_CHROME);

						//arrayList_BrowserTypes.add(str_TestName + "#" +str_UserID + "#SAUCE_CHROME"  + "#" + str_SAUCE_CHROME);
					}
			
				} //if (str_Execute_TS.trim().equalsIgnoreCase("Y"))
				
			} //for (int iRow_TS=1;iRow_TS<=rowCount_TS;iRow_TS++)
			 
			if (!bln_DataIssue) 
			{
				int int_arraySize = arrayList_BrowserTypes.size();
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Total Combinations Selected...: (generateTestNG)" + int_arraySize + "   ---   "+arrayList_BrowserTypes);
				int int_Instances_To_Run = int_arraySize;
				int int_Iter = 0;
				
			 	//Create a list of XmlTests and add the Xmltest you created earlier to it.
			 	List<XmlTest> myTests = new ArrayList<XmlTest>();
			 	
				 for (int i = 0; i <int_Instances_To_Run; i++) 
				 {
					 //Create an instance of XmlTest and assign a name for it.
					 XmlTest myTest = new XmlTest(mySuite);
					 //myTest.setName("MyTest_fromLoop:"+i);
					 myTest.setName(arrayList_BrowserTypes.get(i));					 
					 failedStepsMap.put(arrayList_BrowserTypes.get(i), 0);
					 passedStepsMap.put(arrayList_BrowserTypes.get(i), 0);
					 
					 //Add any parameters that you want to set to the Test.
					 //testngParams.put("str_Iter_Global", Integer.toString(i));
					 //myTest.setParameters(testngParams);
					 
					 //myTest.addParameter("str_Iter_Global"+i, Integer.toString(i));
					 myTest.addParameter("str_Iter_Global", Integer.toString(i));
					 
					//Create a list which can contain the classes that you want to run.
					 List<XmlClass> myClasses = new ArrayList<XmlClass> ();
					 myClasses.add(new XmlClass("testScripts.driverScript"));				 
					 
					//Assign that to the XmlTest Object created earlier.
					 myTest.setXmlClasses(myClasses);
					 
					//Create a list of XmlTests and add the Xmltest you created earlier to it.
					 myTests.add(myTest);
				 }
					 
				 //System.out.println("testngParams: " + testngParams);
				 
				//add the list of tests to your Suite.
				 mySuite.setTests(myTests);
				 
				//Add the suite to the list of suites.
				 List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
				 mySuites.add(mySuite);
				 
				//Set the list of Suites to the testNG object you created earlier.
				 myTestNG.setXmlSuites(mySuites);
				 
				//invoke run() - this will run your class.
				 myTestNG.run();
				
			}
			 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}










//pom


<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.java</groupId>
  <artifactId>Glis2Automation</artifactId>
  <version>0.0.1-SNAPSHOT</version>
	<build>
		<!-- <sourceDirectory>src/test/java/com</sourceDirectory>
		<resources>
			<resource>
				<directory>src/test/java/com</directory>
				<excludes>
					<exclude>**/*.java</exclude>
				</excludes>
			</resource>
		</resources> -->
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
					<fork>true</fork>
			 		<executable>C:\Working\Java\jdk1.8.0_112\bin\javac.exe</executable>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.17</version>
				<inherited>true</inherited>
				<configuration>
					<!-- <properties>
						<property>
							<name>usedefaultlisteners</name>
							<value>false</value>
						</property>
						<property>
							<name>listener</name>
							<value>org.uncommons.reportng.HTMLReporter,
								org.uncommons.reportng.JUnitXMLReporter</value>
						</property>
					</properties> -->
					<workingDirectory>target</workingDirectory>
					<suiteXmlFiles>
<!-- 						<suiteXmlFile>${basedir}\testng.xml</suiteXmlFile>
 -->						<suiteXmlFile>testng.xml</suiteXmlFile>
					</suiteXmlFiles>
				</configuration>
			</plugin>
			<!-- <plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.5</version>
			</plugin> -->
		</plugins>
		<!-- <pluginManagement>
			<plugins>
				This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself.
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.apache.maven.plugins
										</groupId>
										<artifactId>
											maven-compiler-plugin
										</artifactId>
										<versionRange>[3.1,)</versionRange>
										<goals>
											<goal>compile</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement> -->
	</build>

	<dependencies>
		<!-- https://mvnrepository.com/artifact/org.apache.poi/poi -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>3.9</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/commons-codec/commons-codec -->
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.10</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.poi/poi-ooxml -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>3.9</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>3.3.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.pdfbox/pdfbox -->
		<dependency>
			<groupId>org.apache.pdfbox</groupId>
			<artifactId>pdfbox</artifactId>
			<version>1.3.1</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.uncommons/reportng -->
		<dependency>
			<groupId>org.uncommons</groupId>
			<artifactId>reportng</artifactId>
			<version>1.1.4</version>
			<scope>compile</scope>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.google.inject/guice -->
		<dependency>
			<groupId>com.google.inject</groupId>
			<artifactId>guice</artifactId>
			<version>3.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/junit/junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.4</version>
			<scope>compile</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.testng/testng -->
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.8.8</version>
			<scope>compile</scope>
		</dependency>
		<!-- <dependency>
		  <groupId>org.apache.maven.surefire</groupId>
		  <artifactId>surefire</artifactId>
		  <version>2.21.0</version>
		  <type>pom</type>
		</dependency> -->
	</dependencies>
<name>com.src.testScripts</name>
</project>

											
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												





